// function Calc (number) {
//     this.number = number
//     sum = () => {this.number = this.number + 1}
//     sub = () => {this.number = this.number - 1}
// }

// const calc = new Calc (0);
// sum()
// console.log (calc.number)

//------------------2

// class Calc {
//     #number;
//     constructor(number) {
//         this.#number = number
//     }
//     sum = () => { this.number = this.number + 1 }
//     sub = () => { this.number = this.number - 1 }
//     get number () {
//         return this.#number
//     }
//     set number (value) {
//         this.#number = value
//     }
// }

// const calc = new Calc (0);
// calc.sum();
// console.log (calc.number)

//------------------3

class Human {
    age;
    oneEat;
    maxEat = 100;
    nowEat = 0;
    constructor (age,oneEat){
        this.age = age;
        this.oneEat = oneEat;
    }

    eat = () => {
        if (this.nowEat + this.oneEat <= this.maxEat) {
            return this.nowEat = this.nowEat + this.oneEat
        }
    }
}
class Woman extends Human {
    size;
    constructor (age,oneEat,size){
        super(age,oneEat);
        this.size = size;
    }
}

class Men extends Human {
    strong;
    constructor (age,oneEat,strong){
        super(age,oneEat);
        this.strong=strong;
    }
}

const woman = new Woman(23,4,5);
const men = new Men(25,3,6);
woman.eat();
men.eat();
console.log(men);
console.log(woman);
